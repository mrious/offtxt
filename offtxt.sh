#!/bin/bash
#offtxt.sh
#A lovely script
#By Keith Guske

#The following are dependencies to this script
#swftools
#imagemagick
#pdftk
#wget

echo Downloading the swfs
Baseurl='' #Change with the baseurl of where you are downloading the .swf files from
Flash='.swf'
Image='.png'
Pdf='.pdf'
Output='offline'$Pdf

#Download all of the Shockwave files
#Read the seq manpage for more information about the range
for i in $(seq -f "%04g" 1 999)
do
    Full=$Baseurl$i$Flash
    wget $Full
done

#Render the Shockwave files as images
for i in $(seq -f '%04g' 1 999)
do
    swfrender $i$Flash -r 200 -o $i$Image
    echo Rendered $i
done

#Convert the images into PDFs using Imagemagick
for i in $(seq -f '%04g' 1 999)
do
    convert $i$Image $i$Pdf
    echo pdfized $i
done

#Merge all of the PDFs together
pdftk *.pdf cat output $Output

echo All done. Your Pdf can be found as $Output