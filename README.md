# Purpose

The purpose of this project is to allow for the offline use of [VHL Central](https://vistahigherlearning.com/) textbooks

# How to use

In the VHL ebook, find the base url of the Shockwave files using your browser inspector's network tool. Set the $Baseurl varbable in the shell file to this url. Then run offtxt as follows:
```
bash offtxt.sh
```
This will download all the Shockwave files from the provided base url, render them as PDFs, then merge them into one PDF as "offline.pdf"

[I'm updating this file so Gitlab doesn't delete this repo](https://www.theregister.com/2022/08/05/gitlab_reverses_deletion_policy/)
